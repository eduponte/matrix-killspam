module.exports = {
  userName: '@myuser:matrix.example.com',
  accessToken: 'go in your settings in riot, scroll to "Advanced", click to show "Access Token" and paste that here',
  allRooms: ['#publicroom1:matrix.example.com', '!dapOIFDPAOIadfpoai:matrix.example.com', '#publicroom2:matrix.example.com'],
  allAuthors: ['@spammer1:matrix.example.com', '@spammer2:matrix.example.com'],
  allRegex: [/snake oil 1000€/, /buy viagra/],
  historyLimit: 200
};

